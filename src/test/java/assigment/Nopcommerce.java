package assigment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Nopcommerce {
	WebDriver driver;
	 @BeforeClass
	    void setUp() {
	        driver = new ChromeDriver();
	        driver.manage().window().maximize();
	    }

	    /* Quit the driver after execution */
	  /*  @AfterClass
	    void tearDown() {
	        driver.quit();
	    }*/
  @Test(priority=1)
  public void login() 
  {
	  driver.get("https://admin-demo.nopcommerce.com/login");
	  WebElement email = driver.findElement(By.id("Email"));
	  email.clear();
	  email.sendKeys("admin@yourstore.com");
	    
	  WebElement password=driver.findElement(By.id("Password"));
	  password.clear();
	  
	  password.sendKeys("admin");
	  driver.findElement(By.xpath("//button[@type='submit']")).click();
	  String actual=driver.findElement(By.linkText("John Smith")).getText();
	  System.out.println(actual);
	  String expected="John Smith";
	  Assert.assertEquals(actual, expected);
	  
  }
 @Test(priority=2)
  public void Categories() throws IOException
  {
	//System.out.println("Categories");
	  driver.findElement(By.partialLinkText("Catalog")).click();
	  driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	  driver.findElement(By.xpath(" //p[contains(text(),' Categories')]")).click();
	  driver.findElement(By.partialLinkText("Add new")).click();
	  File f=new File("/home/syed/Documents/YourShop/TestData1.xlsx");
		FileInputStream fis=new FileInputStream(f);
		XSSFWorkbook workbook=new XSSFWorkbook(fis);
		XSSFSheet sheet=workbook.getSheetAt(0);
		int rows=sheet.getPhysicalNumberOfRows();
		for(int i=1;i<rows;i++)
		{
			String name=sheet.getRow(i).getCell(0).getStringCellValue();
			String description=sheet.getRow(i).getCell(1).getStringCellValue();
			//String pricefrom = sheet.getRow(i).getCell(2).getStringCellValue();
			//String priceto = sheet.getRow(i).getCell(3).getStringCellValue();
			driver.findElement(By.id("Name")).sendKeys(name);
			driver.findElement(By.id("Description_ifr")).sendKeys(description);
			
			WebElement parentcatalog = driver.findElement(By.id("ParentCategoryId"));
			Select s=new Select(parentcatalog);
			s.selectByVisibleText("Computers >> Build your own computer");
WebElement price = driver.findElement(By.xpath("//label[@for=\"PriceFrom\"]"));
			
			JavascriptExecutor js=(JavascriptExecutor)driver;
			js.executeScript("arguments[0].scrollIntoView()",price);
			
			//driver.findElement(By.id("PriceFrom")).sendKeys(pricefrom);
			//driver.findElement(By.id("PriceTo")).sendKeys(priceto);
			 WebElement savebtn = driver.findElement(By.xpath("//button[@name='save']"));
			 js.executeScript("arguments[0].scrollIntoView(true)", savebtn);
			 savebtn.click();
			 driver.findElement(By.id("SearchCategoryName")).sendKeys("NargeesLaptop");
	          driver.findElement(By.id("search-categories")).click();
	                 	  
	          WebElement element = driver.findElement(By.xpath("(//td[contains(text(),'NargeesLaptop')])[1]"));
	          String text=element.getText();
	          Assert.assertEquals("NargeesLaptop", text);
			
		}

  }

 @Test(priority=3)
  public void Products() {
	 driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	  driver.findElement(By.partialLinkText("Catalog")).click();
	  
	  driver.findElement(By.xpath("//p[contains(text(),'Products')]")).click();
	  driver.findElement(By.id("SearchProductName")).sendKeys("Build your own computer");
	  driver.findElement(By.id("search-products")).click();
	  WebElement element = driver.findElement(By.xpath("//td[text()='Build your own computer']"));
      String text=element.getText();
      Assert.assertEquals("Build your own computer", text);
	  

}
  @Test(priority=4)
  public void Manufacturers() throws IOException {
	  driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	  driver.findElement(By.partialLinkText("Catalog")).click();
	  driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
      driver.findElement(By.xpath("//p[contains(text(),'Manufacturers')]")).click();
            driver.findElement(By.partialLinkText("Add new")).click();
      File f= new File("/home/syed/Documents/YourShop/TestData1.xlsx");
      FileInputStream fis=new FileInputStream(f);
		XSSFWorkbook workbook=new XSSFWorkbook(fis);
		XSSFSheet sheet=workbook.getSheetAt(1);
		int rows=sheet.getPhysicalNumberOfRows();
		for(int i=1;i<rows;i++)
		{
			String name=sheet.getRow(i).getCell(0).getStringCellValue();
			String description=sheet.getRow(i).getCell(1).getStringCellValue();
          driver.findElement(By.id("Name")).sendKeys(name);

          driver.findElement(By.id("Description_ifr")).sendKeys(description);

          driver.findElement(By.xpath("//button[@name='save']")).click();
          driver.findElement(By.id("SearchManufacturerName")).sendKeys("NargeesDell");
          driver.findElement(By.id("search-manufacturers")).click();
          WebElement element = driver.findElement(By.xpath("//td[text()='NargeesDell']"));
          String text=element.getText();
          Assert.assertEquals("NargeesDell", text);
		}
  
}
  
  
  @Test(priority=5)
  public void logOut() {
	  driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	driver.findElement(By.linkText("Logout")).click();
	//driver.quit();

}
} 
  
  
  
  
